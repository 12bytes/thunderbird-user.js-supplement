/**
 *   _ | _  )  _ ) \ \  / __ __| __|   __|      _ \  _ \   __|
 *     |   /   _ \  \  /     |   _|  \__ \     (   |   /  (_ |
 *    _| ___| ___/   _|     _|  ___| ____/ _) \___/ _|_\ \___|
 *                 USER-OVERRIDES.JS FOR HORLOGESKYNET USER.JS
 */

/**
 * name     : 12bytes user-overrides.js for HorlogeSkynet's Thunderbird user.js
 * descrip. : this file may be appended to the 'HorlogeSkynet' user.js for
 *          : Thunderbird
 * version  : 128r2
 * author   : 12bytes.org
 * code     : https://codeberg.org/12bytes/thunderbird-user.js-supplement
 * website  : HorlogeSkynet/thunderbird-user.js:
 *          : https://github.com/HorlogeSkynet/thunderbird-user.js
 * website  : The Thunderbird Privacy Guide for Dummies!:
 *          : https://12bytes.org/the-thunderbird-privacy-guide-for-dummies/
 * credit   : arkenfox/user.js: https://github.com/arkenfox/user.js
 *
 * NOTE TO SELF: search for *TODO*
 */

/*
 * !!! IMPORTANT !!!        HOW TO WORK WITH THIS FILE         !!! IMPORTANT !!!
 * =============================================================================
 *
 * this file is an optional supplement that may be apended to the 'HorlogeSkynet'
 * user.js and used in conjunction with the 'The Thunderbird Privacy Guide for
 * Dummies!' (https://12bytes.org/the-thunderbird-privacy-guide-for-dummies/).
 *
 * the versioning scheme for this file is 'NrN' where the first 'N' is a
 * number corresponding to the major version of Thunderbird for which this file is
 * intended, the 'r' stands for 'revision', and the last 'N' is the revision
 * number, so '999r3' would indicate this user-overrides file is for Thunderbird
 * version 999.x and it is the 3rd revision of the file.
 *
 * some preferences, or entire sections, may be tagged with one or more of the
 * following:
 *
 * [SET].............the value must be checked!!!
 * [UNBREAK=(value)].least likely to cause web breakage but more likely to
 * compromise privacy
 * [SAFE=(value)]....a safe value
 * [PRIV=(value)]....a value which is more protective of privacy but may cause
 * web breakage
 *
 * suggested values are marked with an asterisk (ex: [*PRIV=(value)] ).
 *
 * THIS FILE CONTAINS MY PERSONAL SETTINGS, SOME OF WHICH MAY NOT WORK FOR YOU
 * AND THEREFORE IT IS IMPORTANT TO GO THROUGH IT AND MAKE THE REQUIRED CHANGES.
 * AT A MINIMUM YOU SHOULD EVALUATE ALL SECTIONS AND PREFERENCES TAGGED WITH
 * [SET].
 *
 * TO MAKE UPDATING THIS FILE EASIER, DO NOT EDIT ANY EXISTING PREFERENCES
 * EXCEPT THOSE IN THE CUSTOM PREFERENCES SECTION. instead, copy the entire line
 * you want to change in this file, or the 'HorlogeSkynet' user.js file, to the
 * "USER CUSTOM PREFERENCES" section below and change the preference value
 * there, then when you update this file you can replace everything except your
 * custom preferences.
 *
 * CUSTOM PREFERENCES THAT YOU ADD AND LATER REMOVE WILL REMAIN ACTIVE IN
 * PREFS.JS. to reset/remove a custom preference, comment it out by preceding it
 * with 2 forward slashes (//).
 *
 * WHEN YOU ARE FINISHED EDITING, append this file to the 'HorlogeSkynet' user.js
 * using the updater script, then run the prefsCleaner script provided in the
 * 'arkenfox' repository (make sure Thunderbird is closed). see:
 * https://github.com/arkenfox/user.js/wiki/3.4-Apply-&-Update-&-Maintain
 *
 * AFTER MAKEING ANY CHANGES YOU MUST PERFORM THE FOLLOWING INTEGRITY CHECKS!
 * (you may want to disable your network connection):
 *
 * INTEGRITY CHECK 1: start Thunderbird and open the Error Console from the
 * Developer Tools menu (Ctrl+Shift+J might work) and check for any error
 * messages related to preferences. to find such errors, filter the output
 * using "user.js". following is a sample of what you might see in the console
 * if an error is found:
 *
 * /home/[user]/.mozilla/[profile name]/user.js:[line no.]: prefs parse error: [error description]
 *
 * [line no.] will be a line number corresponding to the line in the
 * 'HorlogeSkynet' user.js where the error lies, but you need to correct the
 * error here, in this file, and not worry about the user.js file. after the
 * error is corrected, run the 'HorlogeSkynet' updater script again and repeat
 * the check.
 *
 * INTEGRITY CHECK 2: after starting Thunderbird with no tabs loaded other than
 * the default, open its Setings dialog and in the General section, click the
 * 'Advanced' button followed by the 'Config Editor' button. next, search for
 * the "_user.js.parrot" troubleshooting preference and check that its value is
 * "SUCCESS! 12BYTES.ORG USER-OVERRIDES SETTINGS LOADED". if it is not then
 * there is a syntax error somewhere in this file in which case you need to
 * search this file for the value of the "_user.js.parrot" troubleshooting
 * preference you saw in the config editor. the error will be between that point
 * and the very next "_user.js.parrot" troubleshooting preference. the following
 * regular expression may help to locate the error. the expression should
 * highlight all lines starting with 'user_pref' except those containing errors.
 *
 * ^user_pref\("\S+", (true|false|"[^"]*"|[0-9.]+)\);(\s|\n)
 */

/**
 * === HorlogeSkynet ACTIVE DIFFS ===
 *
 * [SET] these prefs are duplicates of *active* 'HorlogeSkynet' user.js prefs
 * where the values were changed
 *
 * if the value of the "_user.js.parrot" pref in about:config is "syntax error @
 * HorlogeSkynet DIFFS" then there is a syntax error between this point and the
 * very next "_user.js.parrot" pref
 */

user_pref("_user.js.parrot", "syntax error @ HorlogeSkynet DIFFS"); // do not edit
/**/
user_pref("browser.display.use_system_colors", true);                   // whether to use the system color theme
user_pref("calendar.timezone.local", "America/New_York");               // set to "" to allow TB to aquire local time zone, or "UTC", or your local timezone
user_pref("devtools.debugger.remote-enabled", true);                    // [*SAFE=false] whether to enable remote debugging (needed for developer tools)
user_pref("mail.collect_email_address_outgoing", true);                 // whether to save outgoing mail address in the "Collected Address" address book
user_pref("mail.phishing.detection.enabled", false);                    // whether to enable phishing detection
user_pref("mailnews.headers.showUserAgent", true);                      // whether to display the user-agent string of the senders email client
user_pref("mailnews.start_page.enabled", false);                        // whether to display the default start page before a mail is selected
user_pref("mailnews.start_page.url", "");                               // redundant, just in case - see: "mailnews.start_page.enabled"
//user_pref("privacy.resistFingerprinting", false);                       // [*PRIV=true] whether to enable anti-fingerprinting - 'true' *may* break dates/times in mails and calandar(?) and color options for mail preview pane, options pages, etc. - if JS is disabled, most fingerprinting techniques should be thwarted even if this set to 'false'
user_pref("privacy.userContext.enabled", false);                        // whether to enable containers
user_pref("privacy.userContext.ui.enabled", false);                     // whether to enable the UI for containers
user_pref("security.external_protocol_requires_permission", false);     // [*SAFE=true] whether to prompt when opening a link in an external program

/**
 * -----------------------
 * USER CUSTOM PREFERENCES
 * -----------------------
 */

/**
 * !!! IMPORTANT !!!   !!! IMPORTANT !!!   !!! IMPORTANT !!!   !!! IMPORTANT !!!
 * =============================================================================
 *
 * TO RESET/REMOVE/DELETE A PREFERENCE:
 * ------------------------------------
 * 1. exit Thunderbird
 * 2. comment out the preference(s) by prefixing it with 2 forward slashes (//)
 *    and save your changes (do not move it to the DEPRECIATED/REMOVED PREFS
 *    section below)
 * 3. copy this entire file to the end of the 'HorlogeSkynet' user.js replacing
 *    the existing override code which starts with the 12bytes.org logo
 * 4. run the 'arkenfox' prefsCleaner script
 *
 * TO CHANGE THE VALUE OF A PREFERENCE:
 * ------------------------------------
 * 1. exit Thunderbird
 * 2. copy the entire preference line to the CUSTOM CODE section below
 * 3. change the preference value and save your changes
 * 4. copy this entire file to the end of the 'HorlogeSkynet' user.js replacing
 *    the existing override code which starts with the 12bytes.org logo
 *
 * TO FIND THE DEFAULT VALUE OF A PREFERENCE:
 * ------------------------------------------
 * 1. find the preference in the Config Editor from the Tools > Settings >
 * General menu (the Config Editor button is at the bottom of that page)
 * 2. click the 'Reset' button (note that not all preferences are listed in the
 * Config Editor)
 */

/**
 * if the value of the "_user.js.parrot" pref in about:config is "syntax error @
 * USER CUSTOM PREFERENCES" then there is a syntax error between this point and
 * the very next "_user.js.parrot" pref
 */
user_pref("_user.js.parrot", "syntax error @ USER CUSTOM PREFERENCES"); // do not edit
/**/
/**
 * ADD YOUR CUSTOM PREFERENCES BELOW THIS LINE
 * -------------------------------------------
 */

/**
 * misc. prefs
 */
user_pref("accessibility.tabfocus", 3);                             // which elements can be focused using the Tab key - 1=text fields, 2=all form fields except text, 4=links ony (values can be added together)
user_pref("app.update.auto", false);                                // [SET] [*SAFE=true] whether to enable automatic updates (non-Windows)
user_pref("browser.safebrowsing.blockedURIs.enabled", false);       // [SET] [*SAFE=true] it is not suggested to disable these safebrowsing features
user_pref("browser.safebrowsing.downloads.enabled", false);         // [SET] [*SAFE=true] "
user_pref("browser.safebrowsing.malware.enabled", false);           // [SET] [*SAFE=true] "
user_pref("browser.safebrowsing.phishing.enabled", false);          // [SET] [*SAFE=true] "
user_pref("browser.search.update", false);                          // whether to disable search engine plugin updates
user_pref("browser.triple_click_selects_paragraph", false);         // whether to select entire paragraph when text is triple clicked
user_pref("clipboard.plainTextOnly", true);                         // whether to strip text formatting when copying(?)/pasting text
user_pref("extensions.getAddons.cache.enabled", false);             // whether to enable extension metadata (extension detail tab)
user_pref("extensions.update.autoUpdateDefault", false);            // [SET] whether to automatically install extension updates (after checking for updates)
user_pref("general.useragent.compatMode.firefox", true);            // [*PRIV=true] whether to limit sending extra user-agent data
user_pref("image.animation_mode", "none");                          // how to display animated GIF images - none=do not animate, once=play animation once, normal=play the animation normally
user_pref("intl.date_time.pattern_override.time_medium", "hh:mm:ss a"); // force 12 hr. time format for mail and calendar (may only be necessary on Linux)
user_pref("intl.date_time.pattern_override.time_short", "hh:mm a"); // force 12 hr. time format for mail and calendar (may only be necessary on Linux)
user_pref("mail.mdn.report.enabled", false);                        // whether to enable sending return receipts
user_pref("mailnews.database.global.indexer.enabled", false);       // whether to enable the search indexer
user_pref("offline.autoDetect", false);                             // whether to auto-detect if Thunderbird is on/off line
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true); // whether to load user styles from chrome folder
user_pref("view_source.syntax_highlight", true);                    // whether to highlight the source code of a document when viewing it

/**
 * [SET] the following preferences adjusts the smooth scrolling feature of
 * Thunderbird when using a mouse wheel or keyboard keys to scroll
 */
user_pref("general.smoothscroll", true);                            // whether to enable smooth scrolling
user_pref("general.smoothScroll.lines.durationMaxMS", 400);         // smooth the start/end of line scrolling operations in ms (up/down arrow/page keys)
user_pref("general.smoothScroll.lines.durationMinMS", 200);         // smooth the start/end of line scrolling operations in ms (up/down arrow/page keys)
user_pref("general.smoothScroll.mouseWheel.durationMaxMS", 600);    // smooth the start/end of scrolling operations in ms
user_pref("general.smoothScroll.mouseWheel.durationMinMS", 300);    // smooth the start/end of scrolling operations in ms
user_pref("general.smoothScroll.other.durationMaxMS", 400);         // smooth the start/end of other scrolling operations in ms
user_pref("general.smoothScroll.other.durationMinMS", 200);         // smooth the start/end of other scrolling operations in ms
user_pref("general.smoothScroll.pages.durationMaxMS", 400);         // smooth the start/end of page scrolling operations in ms (PgUp/PgDn keys)
user_pref("general.smoothScroll.pages.durationMinMS", 200);         // smooth the start/end of page scrolling operations in ms (PgUp/PgDn keys)
user_pref("mousewheel.acceleration.factor", 10);                    // sets acceleration factor if mouse wheel.acceleration.start > -1
user_pref("mousewheel.acceleration.start", 0);                      // when to apply mouse wheel.acceleration.factor (after how many scroll clicks of mouse wheel) - value must be greater than -1
user_pref("mousewheel.default.delta_multiplier_x", 85);             // sets the x-axis step size
user_pref("mousewheel.default.delta_multiplier_y", 85);             // sets the y-axis step size
user_pref("mousewheel.default.delta_multiplier_z", 85);             // sets the z-axis step size
user_pref("mousewheel.min_line_scroll_amount", 10);                 // if the CSS line height is smaller than this value in pixels, each scroll click will scroll this amount

/*
 * -------------------------------------
 * YOUR CUSTOM CODE GOES ABOVE THIS LINE
 */

/**
 * DEPRECIATED - DO NOT EDIT - these prefs are needed when running the prefsCleaner script
 */
// user_pref("browser.display.background_color", "");
// user_pref("browser.display.foreground_color", "");
// user_pref("msgcompose.background_color", "#2C2C31");                // [SET] compose mail background color
// user_pref("msgcompose.default_colors", false);                      // [SET] whether to use default colors when composing mail
// user_pref("msgcompose.text_color", "#D4D4DB");                      // [SET] compose mail text color

/**
 * !!! IMPORTANT !!!   !!! IMPORTANT !!!   !!! IMPORTANT !!!   !!! IMPORTANT !!!
 * =============================================================================
 *
 * below is the "_user.js.parrot" preference you must check in Thunderbird's
 * Config Editor - if the value is "SUCCESS! 12BYTES.ORG USER-OVERRIDES SETTINGS
 * LOADED" then there was no syntax error above
 */
user_pref("_user.js.parrot", "SUCCESS! 12BYTES.ORG USER-OVERRIDES SETTINGS LOADED"); // do not edit
