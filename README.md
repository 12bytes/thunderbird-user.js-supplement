# 12bytes.org user-overrides.js for HorlogeSkynet's Thunderbird user.js


## IMPORTANT

**this user-overrides.js is an optional supplement intended to be appended to the [HorlogeSkynet user.js](https://github.com/HorlogeSkynet/thunderbird-user.js)** - a security and privacy centric user preferences file for Thunderbird and its derivatives

the preferences in this user-overrides.js are my personal preferences which are not edited for public consumption, therefore you will need to adjust as necessary

## IMPLEMENTATION

1. add the [HorlogeSkynet user.js](https://github.com/HorlogeSkynet/thunderbird-user.js) file to your Thunderbird profile
2. download the [user-overrrides.js](https://codeberg.org/12bytes/thunderbird-user.js-supplement/raw/branch/master/user-overrides.js) file from this repo to your Thunderbird profile directory
3. edit the settings in the user-overrides.js file to suit your personal preferences
4. copy the entire contents of the user-overrides.js file and paste it at the end of the user.js file

## UPDATE NOTIFICATIONS

* to be notified of updates to the 'HorlogeSkynet' user.js, subscribe to the [commits feed](https://github.com/HorlogeSkynet/thunderbird-user.js/commits/master.atom)
* to be notified of updates to the user-overrides.js in this repository, [subscribe to the RSS feed](https://codeberg.org/12bytes/thunderbird-user.js-supplement.rss) for this repository
* to see the changes between versions, click on the [commits](https://codeberg.org/12bytes/thunderbird-user.js-supplement/commits/branch/master) link, then click on the latest version to compare it with the previous version
